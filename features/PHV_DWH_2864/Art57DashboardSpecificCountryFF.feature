#Each feature file contains one feature
#  Feature files use Gherkin language - business language
Feature: PHV_DWH_2864 - Art57 Publication Dashboard (support for former type Ia variation support)

  Background: Common login steps
    Given User need to be on OBIEE login page
    When user enters username "silioni_e"
    And user enters password "Summer2018!"
    And user clicks login button
    And user navigated to home page
#
  Scenario Outline: Art57 Publication Dashboard - Tab Product Details
    When Navigate to Dashboard
    And Open Art57 Data Query Library
    And Click Art57 Publication support for former type Ia variation support
    Then Opens Art57 Publication support for former type Ia variation
    Then Navigate to Product Details Tab
    And select "<active_substance>" and "<authorisation_country>"
    And Report should results of authorization country


    Examples:
    |  active_substance | authorisation_country |
    | CANAGLIFLOZIN | Denmark |
    | CANAGLIFLOZIN | European Union |
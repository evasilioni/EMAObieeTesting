#Each feature file contains one feature
#  Feature files use Gherkin language - business language
Feature: PHV_DWH_2855 - MAH eRMR line listing: remove hidden columns from criteria

  Background: Common login steps
    Given User need to be on OBIEE login page

  Scenario Outline: Columns of Line listing to be removed
    When  user enters username "<username>"
    And user enters password "<password>"
    And user clicks login button
    Then user navigated to home page
    Then Navigate to Dashboard
    Then Navigate to MAH Pharmacovigilance Query Library
    Then Open MAH Pharmacovigilance Queries
    And Navigate to Line Listing tab
    Then select "<active_substance>"
    And Report should run by click Line listing
    Then Specified columns removed

    Examples:
    | username | password | active_substance |
    | silioni_e | Summer2018! | CANAGLIFLOZIN |
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pages.HomePage;
import pages.LoginPage;
import utilities.DriverFactory;

public class LoginObieeTest {

    WebDriver driver = DriverFactory.open("chrome");

    @Test
    public void loginObieeTest(){
        String webUrl = "http://bi-test.eudra.org/analytics/saw.dll?bieehome";

        driver.get(webUrl);

        // 2. Enter login information (login page)
        LoginPage loginPage = new LoginPage(driver);
        loginPage.setUserName("silioni_e");
        loginPage.setPassword("Summer2018!");
        loginPage.clickSubmit();

        // 3.Get Confirmation (Home page)
        HomePage homePage = new HomePage(driver);
        String title = homePage.title();

        //Assertions
        Assert.assertTrue(title.contains("Oracle BIEE Home"));

//         4.Close the driver
        driver.quit();

    }
}

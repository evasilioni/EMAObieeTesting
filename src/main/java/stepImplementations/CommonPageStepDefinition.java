package stepImplementations;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.HomePage;
import pages.LoginPageFactory;

public class CommonPageStepDefinition extends AbstractPageStepDefinition{

    LoginPageFactory loginPage;
    WebDriver driver = getDriver();
    WebDriverWait wait;

    @Before
    public void setUp(){
        wait = new WebDriverWait(driver, 10);
    }

    @Given("^User need to be on OBIEE login page$")
    public void userNeedToBeOnOBIEELoginPage() throws Throwable {
        System.out.println("User is on the OBIEE login page");
        loginPage = new LoginPageFactory(driver);
    }

    @When("^user enters username \"([^\"]*)\"$")
    public void userEntersUsername(String userName) throws Throwable {
        loginPage.setUserNameText(userName);
    }

    @And("^user enters password \"([^\"]*)\"$")
    public void userEntersPassword(String password) throws Throwable {
        loginPage.setPasswordText(password);
    }

    @And("^user clicks login button$")
    public void userClicksLoginButton() throws Throwable {
        loginPage.clickLogin();
    }

    @Then("^user navigated to home page$")
    public void userNavigatedToHomePage() throws Throwable {
        HomePage homePage = new HomePage(driver);
        String title = homePage.title();

        Assert.assertTrue(title.contains("Oracle BIEE Home"));
    }

    @Then("^Navigate to Dashboard")
    public void NavigateToDashboard() throws Throwable {
        driver.findElement(By.id("dashboard")).click();
        Thread.sleep(1000);
    }


    @After
    public void tearDown() throws InterruptedException {
        if(driver.findElement(By.id("logout")).isDisplayed()){
            driver.findElement(By.id("logout")).click();
        }

        isAlertPresent();
        driver.quit();
        driver = null;
    }

    private boolean isAlertPresent(){
        try{
            driver.switchTo().alert().accept();
            return true;
        }catch (NoAlertPresentException ex){
            return false;
        }
    }
}

package stepImplementations;

import org.openqa.selenium.WebDriver;
import utilities.DriverFactory;

public class AbstractPageStepDefinition {

    protected static WebDriver driver;
    String webUrl = "http://bi-test.eudra.org/analytics/saw.dll?bieehome";

    protected WebDriver getDriver() {
        if( driver == null){
            driver = DriverFactory.open("chrome");
            driver.get(webUrl);
            driver.manage().window().maximize();
        }

        return driver;
    }
}

package stepImplementations;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;


public class MahRemoveColumnsSD extends AbstractPageStepDefinition {

    WebDriver driver = getDriver();

    @Then("^Navigate to MAH Pharmacovigilance Query Library$")
    public void navigateToMAHPharmacovigilanceQueryLibrary() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@foldername='MAH Pharmacovigilance Query Library']")));
        driver.findElement(By.xpath("//a[@foldername='MAH Pharmacovigilance Query Library']")).click();
    }

    @Then("^Open MAH Pharmacovigilance Queries$")
    public void openMAHPharmacovigilanceQueries()  {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()='MAH Pharmacovigilance Queries']")));
        driver.findElement(By.xpath("//a[text()='MAH Pharmacovigilance Queries']")).click();
    }


    @And("^Navigate to Line Listing tab$")
    public void navigateToLineListingTab() {
        driver.findElement(By.xpath("//td[@title='Line listing']")).click();
    }

    @Then("^select \"([^\"]*)\"$")
    public void select(String active_substance)  {
        driver.findElement(By.cssSelector("input.promptTextField.textFieldHelper")).sendKeys(active_substance);

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,'DropDownValueList')]//div[1]//span[1]//span[text()='CANAGLIFLOZIN']")));
        driver.findElement(By.xpath("//div[contains(@class,'DropDownValueList')]//div[1]//span[1]//span[text()='CANAGLIFLOZIN']")).click();
    }

    @And("^Report should run by click Line listing$")
    public void reportShouldRunByClickLineListing() throws Throwable {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()='Line listing']")));

        driver.findElement(By.xpath("//a[text()='Line listing']")).click();

        Thread.sleep(5000);


    }

    @Then("^Specified columns removed$")
    public void specifiedColumnsRemoved() {
        List<WebElement> headerColumns = driver.findElements(By.xpath("//td[contains(@class, 'PTCHC')]//table//tbody//tr//td"));
        headerColumns.forEach(hc -> {
            Assert.assertNotEquals(hc.getText(), "Reaction HLT");
            Assert.assertNotEquals(hc.getText(), "Reaction HLGT");
            Assert.assertNotEquals(hc.getText(), "Reaction SOC");
            Assert.assertNotEquals(hc.getText(), "Active Substance(High Level)");
            Assert.assertNotEquals(hc.getText(), "Reaction PT");
            Assert.assertNotEquals(hc.getText(), "Reaction LLT");
            Assert.assertNotEquals(hc.getText(), "SMQ Level 1");
            Assert.assertNotEquals(hc.getText(), "SMQ Level 1 Broad");
            Assert.assertNotEquals(hc.getText(), "SMQ Level 1 Narrow");
        });
    }


}

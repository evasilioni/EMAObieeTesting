package stepImplementations;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Art57DashboardSpecificCountrySD extends AbstractPageStepDefinition{

    WebDriver driver = getDriver();

    @And("^Open Art57 Data Query Library$")
    public void openArtDataQueryLibrary() throws Throwable {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@foldername='Art57 Data Query Library']")));

        if (!driver.findElement(By.xpath("//a[text()='Art57 Publication (support for former type Ia variation)']")).isDisplayed()){
            driver.findElement(By.xpath("//a[@foldername='Art57 Data Query Library']")).click();
        }
    }

    @And("^Click Art57 Publication support for former type Ia variation support$")
    public void clickArtPublicationSupportForFormerTypeIaVariationSupport() throws Throwable {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()='Art57 Publication (support for former type Ia variation)']")));
        driver.findElement(By.xpath("//a[text()='Art57 Publication (support for former type Ia variation)']")).click();
    }

    @Then("^Opens Art57 Publication support for former type Ia variation$")
    public void openArtPublicationSupportForFormerTypeIaVariation() {
        String idHeaderTitle = driver.findElement(By.id("idHeaderTitleCell")).getText();

        Assert.assertEquals(idHeaderTitle, "Art57 Publication (support for former type Ia variation)");

        System.out.println("---------------" +idHeaderTitle + "---------------");
    }

    @Then("^Navigate to Product Details Tab$")
    public void navigateToProductDetailsTab() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@title='Product Details']")));
        driver.findElement(By.xpath("//span[@title='Product Details']")).click();
    }

    @And("^select \"([^\"]*)\" and \"([^\"]*)\"$")
    public void selectAnd(String active_substance, String auth_country) {

    }

    @And("^Report should results of authorization country$")
    public void reportShouldResultsOfAuthorizationCountry() throws Throwable {

    }

}

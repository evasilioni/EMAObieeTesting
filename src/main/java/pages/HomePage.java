package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {

    WebDriver driver;

    public String title(){
        return driver.getTitle();
    }

    //Contructor initiallizes the state of the driver
    public HomePage(WebDriver driver) {
        this.driver = driver;
    }
}

package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class LoginPageFactory {

    //Define web elements at class level
    WebDriver driver;

    //Definition of web elements at class level
    @FindBy(id ="username")
    WebElement userNameText;

    @FindBy(id="password")
    WebElement passwordText;

    @FindBy(id="submit")
    WebElement loginButton;

    @FindBy(id="logout")
    WebElement logoutButton;

    //Steps
    public void setUserNameText(String userName) { userNameText.sendKeys(userName);}
    public void setPasswordText(String password) { passwordText.sendKeys(password);}
    public void clickLogin() { loginButton.click();}
    //public void signOut() { if(logoutButton.isDisplayed()) logoutButton.click();}

    //Contructor initiallizes the state of the driver
    public LoginPageFactory(WebDriver driver) {
        this.driver = driver;
        //  Wait 20 Second To Find Element If Element Is Not Present
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 20), this);
    }
}
